# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# File describing how to acquire the Intel math libraries for the
# build of an ATLAS project.
#

# Declare the name of the package:
atlas_subdir( MKL )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# The libraries are only available for 64-bit linux. On everything
# else just bail here.
if( NOT "${CMAKE_HOST_SYSTEM_PROCESSOR}" STREQUAL "x86_64" )
   return()
endif()
if( NOT "${CMAKE_SYSTEM_NAME}" STREQUAL "Linux" )
   return()
endif()

# Declare where to get MKL from.
set( ATLAS_MKL_SOURCE
   "URL;https://cern.ch/atlas-software-dist-eos/externals/oneMKL/l_onemkl_p_2022.0.2.136_offline.sh;https://registrationcenter-download.intel.com/akdlm/irc_nas/18483/l_onemkl_p_2022.0.2.136_offline.sh;URL_MD5;8e971594f802f0cd47f2de765590c741"
   CACHE STRING "The source for (one)MKL" )
mark_as_advanced( ATLAS_MKL_SOURCE )

# The temporary installation directory for the full (one)MKL.
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/MKLInstall" )

# Find the shell executable that we would use to launch the installer.
find_program( ATLAS_MKL_SHELL_EXECUTABLE NAMES bash sh )
mark_as_advanced( ATLAS_MKL_SHELL_EXECUTABLE )
if( NOT ATLAS_MKL_SHELL_EXECUTABLE )
   message( WARNING "Could not find a shell executable! "
      "MKL installation will likely fail." )
endif()

# Decide which directory to pick up the libraries from.
if( "${CMAKE_SIZEOF_VOID_P}" EQUAL "8" )
   message( STATUS "Installing the 64-bit MKL libraries" )
   set( _libDir "${_buildDir}/compiler/latest/linux/compiler/lib/intel64_lin" )
elseif( "${CMAKE_SIZEOF_VOID_P}" EQUAL "4" )
   message( STATUS "Installing the 32-bit MKL libraries" )
   set( _libDir "${_buildDir}/compiler/latest/linux/compiler/lib/ia32_lin" )
else()
   message( WARNING "Unknown platform encountered!" )
   return()
endif()

# Install MKL into the build directory.
ExternalProject_Add( MKL
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_MKL_SOURCE}
   DOWNLOAD_NO_EXTRACT 1
   CONFIGURE_COMMAND "${CMAKE_COMMAND}" -E echo "No configuration for MKL"
   BUILD_COMMAND "${CMAKE_COMMAND}" -E env HOME=${_buildDir}
   "${ATLAS_MKL_SHELL_EXECUTABLE}" <DOWNLOADED_FILE>
   --remove-extracted-files yes -a --action install --silent
   --download-cache "${_buildDir}" --download-dir "${_buildDir}"
   --install-dir "${_buildDir}" --log-dir "${_buildDir}" --eula accept
   --intel-sw-improvement-program-consent decline
   INSTALL_COMMAND "${CMAKE_COMMAND}" -E copy_directory "${_libDir}/"
   "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}" )
ExternalProject_Add_Step( MKL purgeInstall
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous installation results for MKL"
   DEPENDEES download
   DEPENDERS patch )
add_dependencies( Package_MKL MKL )

# Install MKL along with the project.
install( DIRECTORY "${_libDir}/"
   DESTINATION "${CMAKE_INSTALL_LIBDIR}" USE_SOURCE_PERMISSIONS OPTIONAL )
install( FILES "${_buildDir}/mkl/latest/licensing/license.txt"
   DESTINATION . RENAME "mkl-license.txt" OPTIONAL )

# Configure the environment setup module:
configure_file(
   "${CMAKE_CURRENT_SOURCE_DIR}/cmake/AtlasMKLEnvironmentConfig.cmake.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/AtlasMKLEnvironmentConfig.cmake"
   @ONLY )

# Now set up the environment of the package:
set( AtlasMKLEnvironment_DIR
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}"
   CACHE INTERNAL "Location of AtlasMKLEnvironmentConfig.cmake" )
find_package( AtlasMKLEnvironment )
