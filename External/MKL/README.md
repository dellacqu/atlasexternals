Intel Math Kernel Library
=========================

This package picks up the Intel Math Kernel Libraries from AFS when building
the code for a 64-bit linux platform, and sets up some environment variables
for making use of these libraries inside Athena.

The way the MKL libraries are picked up should be changed eventually, not to
rely on AFS for the build.
