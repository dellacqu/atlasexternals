3rd Party Extensions To FastJet
===============================

This package builds the 3rd party FastJet extensions downloaded from
fastjet.hepforge.org.

Note that in order to build this package, the FastJet package must be built in
the same project as well.
