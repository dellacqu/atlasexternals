# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AthenaExternals.
#
+ External/Acts
+ External/CheckerGccPlugins
+ External/CLHEP
+ External/Coin3D
+ External/COOL
+ External/CORAL
+ External/flake8_atlas
+ External/Gaudi
+ External/GPerfTools
+ External/Gdb
+ External/Geant4
+ External/VecGeom
+ External/GeoModel
+ External/GoogleTest
+ External/lwtnn
+ External/MKL
+ External/onnxruntime
+ External/prmon
+ External/PyModules
+ External/Simage
+ External/SoQt
+ External/dSFMT
+ External/triSYCL
+ External/yampl
+ External/nlohmann_json
- .*
